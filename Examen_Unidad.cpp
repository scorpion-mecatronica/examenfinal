/*
	Name: Examen
	Copyright: TecNM ITVer
	Author: Herrera G. Luis G. & Guerrero Z. Pedro J. & Priego L. Erick M. & Castillo V. Emilio
	Date: 27/05/19 06:45
	Description: Sacar el area, perimetro o diagonales de un trapecio, segun el usuario lo desee
*/

//*********************************************INICIA SECCIÓN DE DIRECTIVAS*************************************************************
#include <iostream>
#include <string>
#include<math.h>

using namespace std;

//*********************************************TERMINA SECCIÓN DE DIRECTIVAS*************************************************************

//*********************************************INICIA SECCIÓN DE DECLARACIONES*************************************************************
int opc=0;
int resultado=0;
string msj_solicitud1 ="Ingrese la medida de la base menor: "; //Inicializar variable Msj_Solicitud1(string) para solicitar una medida
string msj_solicitud2 ="Ingrese la medida de la base mayor: "; //Inicializar variable Msj_Solicitud2(string) para solicitar una medida
string msj_solicitud3 ="Ingrese la medida de la altura: "; //Inicializar variable Msj_Solicitud3(string) para solicitar una medida
string msj_solicitud4 ="Ingrese la medida de la los dos lados iguales: "; //Inicializar variable Msj_Solicitud1(string) para solicitar una medida
string msj_resultado = "\nEl area es: ";
string msj_error="Las medidas no corresponden a un trapecio";
//*********************************************TERMINA SECCIÓN DE DECLARACIONES*************************************************************
int main(){
	float medidas[3]; //Se declara el arreglo(vector) de tipo float
	int i=0; //Se inicializa la Variable de Control i a 0
	
	
	do{
		cout<<"\n1. Area"<<endl;
		cout<<"\n2. Perimetro"<<endl;
		cout<<"\n3. Diagonales"<<endl;
		cout<<"\n4. salir"<<endl;
		cout<<"\n Opcion: ";
		cin>>opc;

		switch(opc){
			case 1:  //Se piden datos(medidas) para Area, se muestran y se hace operacion 
			cout<<msj_solicitud1;//Mostrar msj_solicitud1 para pedir la medida de la base mayor
			 while (!(cin>>medidas[0])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud1; //Mostrar msj_solicitud1 para pedir la medida de la base mayor
            } //Termina el while
            
            cout<<msj_solicitud2;//Mostrar msj_solicitud2 para pedir la medida de la base menor
			 while (!(cin>>medidas[1])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud2; //Mostrar msj_solicitud2 para pedir la medida de la base menor
            } //Termina el while
            
            cout<<msj_solicitud3;//Mostrar msj_solicitud1 para pedir la medida de la altura
			 while (!(cin>>medidas[2])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud3; //Mostrar msj_solicitud1 para pedir la medida de la altura
            } //Termina el while
            cout<<endl;
            
            cout<<"Las medidas que usted nos proporciono son:"<<endl;
            for (i=0;i<3;i++)//INICIA EL FOR  i++ es el contador que incrementará hasta el tamaño(TAM) del arreglo
             cout << "La medida No."<<i<<" es "<<medidas[i] << endl; //Se muestran, con un msj, los elementos de de cada indice del arreglo    
             //TERMINA EL FOR 
             if(medidas[0]!=medidas[1]){
             	resultado =((medidas[0]+medidas[1])*medidas[2])/2; //Obtener el valor del area del trapecio con la formula del area de trapecio
                cout << msj_resultado<<resultado << endl; //Mostrar msj_resultado+resultado, para mostrar en pantalla el Area del trapecio
			 }
			 else{
			 	cout<<msj_error<<endl;
			 }
			break;
			case 2:  //Se piden datos para Perimetro, se muestran y se hace operacion Si la base mayor y la base menor son iguales mostrar: no es un trapecio
		cout<<msj_solicitud1;//Mostrar msj_solicitud1 para pedir la medida de la base mayor
			 while (!(cin>>medidas[0])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud1; //Mostrar msj_solicitud1 para pedir la medida de la base mayor
            } //Termina el while
            
            cout<<msj_solicitud2;//Mostrar msj_solicitud2 para pedir la medida de la base menor
			 while (!(cin>>medidas[1])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud2; //Mostrar msj_solicitud2 para pedir la medida de la base menor
            } //Termina el while
            
            cout<<msj_solicitud4;//Mostrar msj_solicitud4 para pedir la medida de los lados iguales
			 while (!(cin>>medidas[2])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud4; //Mostrar msj_solicitud4 para pedir la medida de los lados iguales
            } //Termina el while
            cout<<endl;
            
            cout<<"Las medidas que usted nos proporciono son:"<<endl;
            for (i=0;i<3;i++)//INICIA EL FOR  i++ es el contador que incrementará hasta el tamaño(TAM) del arreglo
             cout << "La medida No."<<i<<" es "<<medidas[i] << endl; //Se muestran, con un msj, los elementos de de cada indice del arreglo    
             //TERMINA EL FOR 
             if(medidas[0]!=medidas[1]){
			 resultado =((medidas[0]+medidas[1])+(medidas[2]*2)); //Obtener el valor del area del trapecio con la formula del area de trapecio
            msj_resultado="\nEl perimetro es: ";
			cout << msj_resultado<<resultado << endl; //Mostrar msj_resultado+resultado, para mostrar en pantalla el perimetro del trapecio
			 }
			 else{
			 	cout<<msj_error<<endl;
			 }
			break;
			case 3: //Se piden datos para Diagonales, se muestran  y se hace operacion Si la base mayor y la base menor son iguales mostrar: no es un trapecio
			cout<<msj_solicitud1;//Mostrar msj_solicitud1 para pedir la medida de la base mayor
			 while (!(cin>>medidas[0])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud1; //Mostrar msj_solicitud1 para pedir la medida de la base mayor
            } //Termina el while
            
            cout<<msj_solicitud2;//Mostrar msj_solicitud2 para pedir la medida de la base menor
			 while (!(cin>>medidas[1])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud2; //Mostrar msj_solicitud2 para pedir la medida de la base menor
            } //Termina el while
            
            cout<<msj_solicitud4;//Mostrar msj_solicitud4 para pedir la medida de los lados iguales
			 while (!(cin>>medidas[2])) //Se almacena el numero en la variable float (si el dato es diferente de float, se efectua el While)
            { //Inicia el while
            cin.clear(); //limpiar lo que se capturo
            fflush(stdin); //Desechar lo que queda(limpiar el buffer)
            cout<<msj_solicitud4; //Mostrar msj_solicitud4 para pedir la medida de los lados iguales
            } //Termina el while
            cout<<endl;
            
            cout<<"Las medidas que usted nos proporciono son:"<<endl;
            for (i=0;i<3;i++)//INICIA EL FOR  i++ es el contador que incrementará hasta el tamaño(TAM) del arreglo
             cout << "La medida No."<<i<<" es "<<medidas[i] << endl; //Se muestran, con un msj, los elementos de de cada indice del arreglo    
             //TERMINA EL FOR 
             
             if(medidas[0]!=medidas[1]){
             	
            resultado= (sqrt((medidas[0]*medidas[1])+(pow(medidas[2],2))));
            msj_resultado="\nLas diagonales son: ";
			cout << msj_resultado<<resultado << endl; //Mostrar msj_resultado+resultado, para mostrar en pantalla el perimetro del trapecio
			 }
			 else{
		    cout<<msj_error<<endl;
			 }break;	
		}		

	}while(opc != 4);
	
	return 0;
}
